#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import os


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        # Recibe mensajes y responde a los clientes

        metodos = {'INVITE', 'BYE', 'ACK'}
        ristra = b"""SIP/2.0 100 Trying\r\n\r\nSIP/2.0 180 Ringing\r\n\r
SIP/2.0 200 OK\r\n\r\n"""

        for line in self.rfile:
            salto = b'\r\n'
            if line == salto:
                continue
            # Leyendo línea a línea lo que nos envía el cliente
            texto = line.decode('utf-8')
            metodo = texto.split(' ')[0]
            print("El cliente nos manda " + texto)
            if metodo in metodos:
                comprobacion = texto.split(' ')[-2]
                comprobacion2 = comprobacion.split(':')
                comprobacion3 = comprobacion2[-1].split('@')
                if comprobacion2[0] != 'sip':
                    self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
                    continue
                elif texto.split(' ')[-1] != 'SIP/2.0\r\n':
                    self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
                    continue

            if metodo == 'INVITE':
                self.wfile.write(ristra)
            elif metodo == 'ACK':
                aEjecutar = 'mp32rtp -i 127.0.0.1 -p 23032 < ' + AUDIO_FILE
                print("Vamos a ejecutar", aEjecutar)
                os.system(aEjecutar)
            elif metodo == 'BYE':
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            else:
                self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")


if __name__ == "__main__":
    """
    Programa principal
    """
    if len(sys.argv) != 4:
        sys.exit('Usage: python3 server.py IP port audio_file')
    DIRIP = sys.argv[1]
    PORT = int(sys.argv[2])
    AUDIO_FILE = sys.argv[3]

    # Creamos servidor de eco y escuchamos
    serv = socketserver.UDPServer(('', PORT), EchoHandler)
    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
