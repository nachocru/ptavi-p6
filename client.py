#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys

# Constantes. Dirección IP del servidor y contenido a enviar

metodos = {'INVITE', 'BYE'}

if len(sys.argv) != 3:
    sys.exit('Usage: python3 client.py method receiver@IP:SIPport')
if sys.argv[1] not in metodos:
    sys.exit('Usage: python3 client.py method receiver@IP:SIPport')


# Cliente UDP simple.

direcciones = sys.argv[2].split('@')[-1]
SERVER = direcciones.split(':')[0]
PORT = int(direcciones.split(':')[-1])
METHOD = sys.argv[1]
RECEPTOR = sys.argv[2].split('@')[0] + '@'

# Contenido que vamos a enviar
LINE = METHOD + ' sip:' + RECEPTOR + SERVER + ' SIP/2.0'

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((SERVER, PORT))

    print("Enviando: " + LINE)
    my_socket.send(bytes(LINE, 'utf-8') + b'\r\n\r\n')
    data = my_socket.recv(1024)
    mes = data.decode('utf-8')
    print(mes)
    if METHOD == 'INVITE':
        if mes == """SIP/2.0 100 Trying\r\n\r\nSIP/2.0 180 Ringing\r\n\r
SIP/2.0 200 OK\r\n\r\n""":
            LINE = 'ACK' + ' sip:' + RECEPTOR + SERVER + ' SIP/2.0'
            print("Enviando: " + LINE)
            my_socket.send(bytes(LINE, 'utf-8') + b'\r\n\r\n')
    print("Terminando socket...")

print("Fin.")
